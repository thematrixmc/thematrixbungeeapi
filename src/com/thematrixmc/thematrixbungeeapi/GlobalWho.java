package com.thematrixmc.thematrixbungeeapi;

import com.google.common.collect.ImmutableSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class GlobalWho extends Command implements TabExecutor {

    public GlobalWho() {
        super("GlobalWho", "bungee.command.globalwho", new String[]{"gw"});
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (args.length == 0) {
            sender.sendMessage(Messages.TAG + ChatColor.DARK_AQUA + "Usage: /globalwho [user]");
            return;
        }
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
        if (player == null) {
            sender.sendMessage(Messages.TAG + ChatColor.GRAY + "Player not online.");
            return;
        }
        try {
            sender.sendMessage(Messages.TAG + ChatColor.GRAY + "Player: " + Ranks.getRankColour(player) + player.getName());
            sender.sendMessage(Messages.TAG + ChatColor.GRAY + "Rank: " + Ranks.getRankColour(player) + Ranks.getRank(player));
            sender.sendMessage(Messages.TAG + ChatColor.GRAY + "Name of server: " + Ranks.getRankColour(player) + ProxyServer.getInstance().getPlayer(player.getName()).getServer().getInfo().getName());
            sender.sendMessage(Messages.TAG + ChatColor.GRAY + "Player count of server: " + Ranks.getRankColour(player) + player.getServer().getInfo().getPlayers().size());
            sender.sendMessage(Messages.TAG + ChatColor.GRAY + "Ping to proxy: " + Ranks.getRankColour(player) + player.getPing());
            if (sender.hasPermission("bungee.globalwho.seeip")) {
                String IP = player.getAddress().getAddress().toString();
                sender.sendMessage(Messages.TAG + ChatColor.GRAY + "IP: " + Ranks.getRankColour(player) + IP);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GlobalWho.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 1 || args.length == 0) {
            return ImmutableSet.of();
        }
        
        Set<String> matches = new HashSet<>();
        if(args.length == 1){
            String search = args[0].toLowerCase();
            for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
                if(all.getName().toLowerCase().startsWith(search)){
                    matches.add(all.getName());
                }
            }
        }
        return matches;
    }
}
