package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class StaffChatOn extends Command{

    public StaffChatOn() {
        super("scon", "bungee.staffchat.toggleon", "staffchaton");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if(args.length > 0){
                player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /staffchaton");
            }else{
                if(TheMatrixBungeeAPI.staffChat.get(player.getName()) == true){
                    player.sendMessage(Messages.TAG + "§cYour staff chat is already toggled on.");
                }else{
                    TheMatrixBungeeAPI.staffChat.put(player.getName(), true);
                    player.sendMessage(Messages.TAG + "§aStaff chat has successfully toggled: §lon.");
                }
            }
        }
    }
    
    

}
