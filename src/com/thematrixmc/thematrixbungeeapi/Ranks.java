package com.thematrixmc.thematrixbungeeapi;

import java.sql.SQLException;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class Ranks {

    public static boolean isStaff(ProxiedPlayer player) throws SQLException{
        return Ranks.getRank(player).equalsIgnoreCase("moderator") | Ranks.getRank(player).equalsIgnoreCase("seniormod") | Ranks.getRank(player).equalsIgnoreCase("admin") | Ranks.getRank(player).equalsIgnoreCase("staffmanager") | Ranks.getRank(player).equalsIgnoreCase("developer") | Ranks.getRank(player).equalsIgnoreCase("owner");
    }
    public static ChatColor getRankColour(ProxiedPlayer player) throws SQLException {
        if(PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("default")) {
            return ChatColor.BLUE;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("iron")) {
            return ChatColor.GRAY;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("gold")) {
            return ChatColor.GOLD;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("diamond")) {
            return ChatColor.AQUA;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("friend")) {
            return ChatColor.DARK_AQUA;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("youtuber")) {
            return ChatColor.LIGHT_PURPLE;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("builder")) {
            return ChatColor.DARK_BLUE;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("moderator")) {
            return ChatColor.RED;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("seniormod")) {
            return ChatColor.DARK_RED;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("admin")) {
            return ChatColor.DARK_PURPLE;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("staffmanager")) {
            return ChatColor.DARK_GREEN;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("developer")) {
            return ChatColor.YELLOW;
        }else
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("owner")) {
            return ChatColor.GREEN;
        } else {
            return ChatColor.BLUE;
        }
    }

    public static String getRank(ProxiedPlayer player) throws SQLException {
        if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("default")) {
            return "Default";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("iron")) {
            return "Iron";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("gold")) {
            return "Gold";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("diamond")) {
            return "Diamond";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("friend")) {
            return "VIP";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("youtuber")) {
            return "Youtuber";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("builder")) {
            return "Seaborgia";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("moderator")) {
            return "Moderator";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("seniormod")) {
            return "Senior Moderator";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("admin")) {
            return "Admin";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("staffmanager")) {
            return "Staff Manager";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("developer")) {
            return "Developer";
        } else if (PermissionsAPI.getBungeeRank(player).equalsIgnoreCase("owner")) {
            return "Owner";
        } else {
            return "Default";
        }
    }

    public static void setBungeePermissions(ProxiedPlayer player, String group) throws SQLException {
        if (group.equalsIgnoreCase("default")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.command.hub", true);
            System.out.println("Permissions for group default were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("iron")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.command.hub", true);
            System.out.println("Permissions for group iron were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("gold")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.command.hub", true);
            System.out.println("Permissions for group gold were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("diamond")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.command.hub", true);
            System.out.println("Permissions for group diamond were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("friend")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.command.hub", true);
            System.out.println("Permissions for group friend were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("youtuber")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.command.hub", true);
            System.out.println("Permissions for group youtuber were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("builder")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungeecord.command.server", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungee.blamealex.receive", true);
            if (player.getName().equalsIgnoreCase("Peter_Forties")) {
                player.setPermission("bungeecord.command.send", true);
            }
            System.out.println("Permissions for group builder were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("moderator")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.whereis", true);
            player.setPermission("bungee.command.staff", true);
            player.setPermission("bungee.command.kick", true);
            player.setPermission("bungee.notify.staff", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.staffchat.toggleon", true);
            player.setPermission("bungee.staffchat.toggleoff", true);
            player.setPermission("bungee.staffchat.notifyon", true);
            player.setPermission("bungee.staffchat.notifyoff", true);
            player.setPermission("bungee.kick.notify", true);
            player.setPermission("bungee.blamealex.receive", true);
            player.setPermission("bungeecord.command.server", true);
            System.out.println("Permissions for group moderator were added to player " + player.getName() + " successfully.");
        }

        if (group.equalsIgnoreCase("seniormod")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.whereis", true);
            player.setPermission("bungee.command.staff", true);
            player.setPermission("bungee.command.kick", true);
            player.setPermission("bungee.notify.staff", true);
            player.setPermission("bungee.command.gsetrank", true);
            player.setPermission("bungee.command.ggetrank", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungee.command.globalsay", true);
            player.setPermission("bungeecord.command.server", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungee.command.globalwho", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungeecord.command.send", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.staffchat.toggleon", true);
            player.setPermission("bungee.staffchat.toggleoff", true);
            player.setPermission("bungee.staffchat.notifyon", true);
            player.setPermission("bungee.staffchat.notifyoff", true);
            player.setPermission("bungee.kick.notify", true);
            player.setPermission("bungee.blamealex.receive", true);
            System.out.println("Permissions for group seniormod were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("admin")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.whereis", true);
            player.setPermission("bungee.command.staff", true);
            player.setPermission("bungee.command.kick", true);
            player.setPermission("bungee.notify.staff", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungee.command.gsetrank", true);
            player.setPermission("bungee.command.ggetrank", true);
            player.setPermission("bungee.command.globalsay", true);
            player.setPermission("bungeecord.command.server", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungee.command.globalwho", true);
            player.setPermission("bungee.globalwho.seeip", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungeecord.command.send", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.staffchat.toggleon", true);
            player.setPermission("bungee.staffchat.toggleoff", true);
            player.setPermission("bungee.staffchat.notifyon", true);
            player.setPermission("bungee.staffchat.notifyoff", true);
            player.setPermission("bungee.kick.notify", true);
            player.setPermission("bungee.blamealex.receive", true);
            System.out.println("Permissions for group admin were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("staffmanager")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.whereis", true);
            player.setPermission("bungee.command.staff", true);
            player.setPermission("bungee.command.kick", true);
            player.setPermission("bungee.notify.staff", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungee.command.gsetrank", true);
            player.setPermission("bungee.command.ggetrank", true);
            player.setPermission("bungee.command.globalsay", true);
            player.setPermission("bungeecord.command.server", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungee.command.globalwho", true);
            player.setPermission("bungee.globalwho.seeip", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungeecord.command.send", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.staffchat.toggleon", true);
            player.setPermission("bungee.staffchat.toggleoff", true);
            player.setPermission("bungee.staffchat.notifyon", true);
            player.setPermission("bungee.staffchat.notifyoff", true);
            player.setPermission("bungee.kick.notify", true);
            player.setPermission("bungee.blamealex.receive", true);
            System.out.println("Permissions for group staffmanager were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("developer")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.gblamealex", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.whereis", true);
            player.setPermission("bungee.command.staff", true);
            player.setPermission("bungee.command.kick", true);
            player.setPermission("bungee.notify.staff", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungee.command.gsetrank", true);
            player.setPermission("bungee.command.ggetrank", true);
            player.setPermission("bungee.command.globalsay", true);
            player.setPermission("bungeecord.command.server", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungee.command.globalwho", true);
            player.setPermission("bungee.globalwho.seeip", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungeecord.command.send", true);
            player.setPermission("bungeecord.command.ip", true);
            player.setPermission("bungeecord.command.end", true);
            player.setPermission("bungeecord.command.*", true);
            player.setPermission("bungee.kick.notify", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.staffchat.toggleon", true);
            player.setPermission("bungee.staffchat.toggleoff", true);
            player.setPermission("bungee.staffchat.notifyon", true);
            player.setPermission("bungee.staffchat.notifyoff", true);
            player.setPermission("bungee.blamealex.receive", true);
            System.out.println("Permissions for group developer were added to player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("owner")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", true);
            player.setPermission("bungee.command.gblamealex", true);
            player.setPermission("bungee.command.ping", true);
            player.setPermission("bungee.command.whereis", true);
            player.setPermission("bungee.command.staff", true);
            player.setPermission("bungee.command.kick", true);
            player.setPermission("bungee.notify.staff", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungee.command.gsetrank", true);
            player.setPermission("bungee.command.ggetrank", true);
            player.setPermission("bungee.command.globalsay", true);
            player.setPermission("bungeecord.command.server", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungee.command.globalwho", true);
            player.setPermission("bungee.globalwho.seeip", true);
            player.setPermission("bungee.command.mmode", true);
            player.setPermission("bungee.command.hub", true);
            player.setPermission("bungeecord.command.send", true);
            player.setPermission("bungeecord.command.ip", true);
            player.setPermission("bungeecord.command.end", true);
            player.setPermission("bungee.kick.notify", true);
            player.setPermission("bungeecord.command.*", true);
            player.setPermission("bungee.command.globallist", true);
            player.setPermission("bungee.staffchat.toggleon", true);
            player.setPermission("bungee.staffchat.toggleoff", true);
            player.setPermission("bungee.staffchat.notifyon", true);
            player.setPermission("bungee.staffchat.notifyoff", true);
            player.setPermission("bungee.blamealex.receive", true);
            System.out.println("Permissions for group owner were added to player " + player.getName() + " successfully.");
        }
    }

    public static void removeBungeePermissions(ProxiedPlayer player, String group) throws SQLException {
        if (group.equalsIgnoreCase("default")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.command.hub", false);
            System.out.println("Permissions for group default were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("iron")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.command.hub", false);
            System.out.println("Permissions for group iron were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("gold")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.command.hub", false);
            System.out.println("Permissions for group gold were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("diamond")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.command.hub", false);
            System.out.println("Permissions for group diamond were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("friend")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.command.hub", false);
            System.out.println("Permissions for group friend were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("youtuber")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.command.hub", false);
            System.out.println("Permissions for group youtuber were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("builder")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungeecord.command.server", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungee.blamealex.receive", false);
            if (player.getName().equalsIgnoreCase("Peter_Forties")) {
                player.setPermission("bungeecord.command.send", false);
            }
            System.out.println("Permissions for group builder were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("moderator")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.whereis", false);
            player.setPermission("bungee.command.staff", false);
            player.setPermission("bungee.command.kick", false);
            player.setPermission("bungee.notify.staff", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.staffchat.toggleon", false);
            player.setPermission("bungee.staffchat.toggleoff", false);
            player.setPermission("bungee.staffchat.notifyon", false);
            player.setPermission("bungee.staffchat.notifyoff", false);
            player.setPermission("bungee.kick.notify", false);
            player.setPermission("bungee.blamealex.receive", false);
            player.setPermission("bungeecord.command.server", false);
            System.out.println("Permissions for group moderator were removed from player " + player.getName() + " successfully.");
        }

        if (group.equalsIgnoreCase("seniormod")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.whereis", false);
            player.setPermission("bungee.command.staff", false);
            player.setPermission("bungee.command.kick", false);
            player.setPermission("bungee.notify.staff", false);
            player.setPermission("bungee.command.gsetrank", false);
            player.setPermission("bungee.command.ggetrank", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungee.command.globalsay", false);
            player.setPermission("bungeecord.command.server", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungee.command.globalwho", false);
            player.setPermission("bungeecord.command.send", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.staffchat.toggleon", false);
            player.setPermission("bungee.staffchat.toggleoff", false);
            player.setPermission("bungee.staffchat.notifyon", false);
            player.setPermission("bungee.staffchat.notifyoff", false);
            player.setPermission("bungee.kick.notify", false);
            player.setPermission("bungee.blamealex.receive", false);
            System.out.println("Permissions for group seniormod were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("admin")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.whereis", false);
            player.setPermission("bungee.command.staff", false);
            player.setPermission("bungee.command.kick", false);
            player.setPermission("bungee.notify.staff", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungee.command.gsetrank", false);
            player.setPermission("bungee.command.ggetrank", false);
            player.setPermission("bungee.command.globalsay", false);
            player.setPermission("bungeecord.command.server", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungee.command.globalwho", false);
            player.setPermission("bungee.globalwho.seeip", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungeecord.command.send", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.staffchat.toggleon", false);
            player.setPermission("bungee.staffchat.toggleoff", false);
            player.setPermission("bungee.staffchat.notifyon", false);
            player.setPermission("bungee.staffchat.notifyoff", false);
            player.setPermission("bungee.kick.notify", false);
            player.setPermission("bungee.blamealex.receive", false);
            System.out.println("Permissions for group admin were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("staffmanager")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.whereis", false);
            player.setPermission("bungee.command.staff", false);
            player.setPermission("bungee.command.kick", false);
            player.setPermission("bungee.notify.staff", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungee.command.gsetrank", false);
            player.setPermission("bungee.command.ggetrank", false);
            player.setPermission("bungee.command.globalsay", false);
            player.setPermission("bungeecord.command.server", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungee.command.globalwho", false);
            player.setPermission("bungee.globalwho.seeip", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungeecord.command.send", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.staffchat.toggleon", false);
            player.setPermission("bungee.staffchat.toggleoff", false);
            player.setPermission("bungee.staffchat.notifyon", false);
            player.setPermission("bungee.staffchat.notifyoff", false);
            player.setPermission("bungee.kick.notify", false);
            player.setPermission("bungee.blamealex.receive", false);
            System.out.println("Permissions for group staffmanager were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("developer")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.gblamealex", false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.whereis", false);
            player.setPermission("bungee.command.staff", false);
            player.setPermission("bungee.command.kick", false);
            player.setPermission("bungee.notify.staff", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungee.command.gsetrank", false);
            player.setPermission("bungee.command.ggetrank", false);
            player.setPermission("bungee.command.globalsay", false);
            player.setPermission("bungeecord.command.server", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungee.command.globalwho", false);
            player.setPermission("bungee.globalwho.seeip", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungeecord.command.send", false);
            player.setPermission("bungeecord.command.ip", false);
            player.setPermission("bungeecord.command.end", false);
            player.setPermission("bungeecord.command.*", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.staffchat.toggleon", false);
            player.setPermission("bungee.staffchat.toggleoff", false);
            player.setPermission("bungee.staffchat.notifyon", false);
            player.setPermission("bungee.staffchat.notifyoff", false);
            player.setPermission("bungee.kick.notify", false);
            player.setPermission("bungee.blamealex.receive", false);
            System.out.println("Permissions for group developer were removed from player " + player.getName() + " successfully.");
        }
        if (group.equalsIgnoreCase("owner")) {
            player.setPermission(player.getPermissions().toString(), false);
            player.setPermission("bungee.command.gblamealex", false);
            player.setPermission("bungee.command.whereami", false);
            player.setPermission("bungee.command.ping", false);
            player.setPermission("bungee.command.whereis", false);
            player.setPermission("bungee.command.staff", false);
            player.setPermission("bungee.command.kick", false);
            player.setPermission("bungee.command.hub", false);
            player.setPermission("bungee.notify.staff", false);
            player.setPermission("bungee.command.gsetrank", false);
            player.setPermission("bungee.command.ggetrank", false);
            player.setPermission("bungee.command.globalsay", false);
            player.setPermission("bungeecord.command.server", false);
            player.setPermission("bungee.command.mmode", false);
            player.setPermission("bungee.command.globalwho", false);
            player.setPermission("bungee.globalwho.seeip", false);
            player.setPermission("bungeecord.command.send", false);
            player.setPermission("bungeecord.command.ip", false);
            player.setPermission("bungeecord.command.end", false);
            player.setPermission("bungeecord.command.*", false);
            player.setPermission("bungee.command.globallist", false);
            player.setPermission("bungee.staffchat.toggleon", false);
            player.setPermission("bungee.staffchat.toggleoff", false);
            player.setPermission("bungee.staffchat.notifyon", false);
            player.setPermission("bungee.staffchat.notifyoff", false);
            player.setPermission("bungee.kick.notify", false);
            player.setPermission("bungee.blamealex.receive", false);
            System.out.println("Permissions for group owner were removed from player " + player.getName() + " successfully.");
        }
    }
}
