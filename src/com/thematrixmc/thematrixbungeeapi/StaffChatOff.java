package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class StaffChatOff extends Command{

    public StaffChatOff() {
        super("scoff", "bungee.staffchat.toggleoff", "staffchatoff");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if(args.length > 0){
                player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /staffchatoff");
            }else{
                if(TheMatrixBungeeAPI.staffChat.get(player.getName()) == false){
                    player.sendMessage(Messages.TAG + "§cYour staff chat is already toggled off.");
                }else{
                    TheMatrixBungeeAPI.staffChat.put(player.getName(), false);
                    player.sendMessage(Messages.TAG + "§cStaff chat has successfully toggled: §loff.");
                }
            }
        }
    }
    
    

}
