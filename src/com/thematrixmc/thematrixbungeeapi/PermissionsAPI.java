package com.thematrixmc.thematrixbungeeapi;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PermissionsAPI {
    
    public static String getBungeeRank(ProxiedPlayer p) throws SQLException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM `BungeeRanks` WHERE `UUID`='" + p.getUniqueId().toString() + "';");
            if (!rs.next()){
                return null;
            } 
            return rs.getString("Rank");
    }
    
    public static void setRank(ProxiedPlayer p, String rank) throws SQLException, ClassNotFoundException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement();
                String getRank = getBungeeRank(p);
                if (getRank != null) {
                    Ranks.removeBungeePermissions(p, getRank);
                    statement.executeUpdate("UPDATE `BungeeRanks` SET `Rank`='" + rank + "' WHERE `UUID`='" + p.getUniqueId().toString() + "';");
                    Ranks.setBungeePermissions(p, rank);
                } else {
                    statement.executeUpdate("INSERT INTO `BungeeRanks` (`UUID`,`Rank`,`Name`) VALUES ('" + p.getUniqueId().toString() + "','" + rank + "', '" + p.getName() + "');");
                    Ranks.setBungeePermissions(p, rank);
                }
    }
    
    public static void setOfflineRank(String p, String rank) throws SQLException, ClassNotFoundException {
        Statement statement = TheMatrixBungeeAPI.conn.createStatement(); 
        Statement query = TheMatrixBungeeAPI.conn.createStatement();
        String sqlRank;
        ResultSet rs = query.executeQuery("SELECT * FROM `BungeeRanks` WHERE `Name`='" + p + "';");
        if(!rs.next()){
            sqlRank = null;
        }else{
            sqlRank = rs.getString("Rank");
        }
        if (sqlRank != null) {
            statement.executeUpdate("UPDATE `BungeeRanks` SET `Rank`='" + rank + "' WHERE `Name`='" + p + "';");
        } else {
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(p);
            statement.executeUpdate("INSERT INTO `BungeeRanks` (`UUID`,`Rank`,`Name`) VALUES ('" + player.getUniqueId().toString() + "','" + rank + "', '" + p + "');");
        }
    }
   
    public static String getOfflineRank(String p) throws SQLException, ClassNotFoundException {
        Statement query = TheMatrixBungeeAPI.conn.createStatement();
        ResultSet rs = query.executeQuery("SELECT * FROM `BungeeRanks` WHERE `Name`='" + p + "';");
        if(!rs.next()){
            return null;
        }else{
            return rs.getString("Rank");
        }
    }
}
