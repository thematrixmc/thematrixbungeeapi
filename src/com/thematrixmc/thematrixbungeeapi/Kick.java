package com.thematrixmc.thematrixbungeeapi;

import com.google.common.collect.ImmutableSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class Kick extends Command implements TabExecutor {

    public Kick() {
        super("kick", "bungee.command.kick", new String[]{"disconnect"});
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(Messages.TAG + ChatColor.RED + "Usage: /kick [user] [reason]");
            return;
        }
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
        if (player == null) {
            sender.sendMessage(Messages.TAG + ChatColor.RED + "Error: Player is not online.");
            return;
        }

        StringBuilder str = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            str.append(args[i]).append(" ");
        }

        if (args.length == 1) {
            player.disconnect(ChatColor.RED + "You have been kicked from the server.");
            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                if (all.hasPermission("bungee.kick.notify")) {
                    try {
                        all.sendMessage(Messages.TAG + Ranks.getRankColour(player) + player.getName() + " §cwas kicked by " + sender.getName() + ".");
                    } catch (SQLException ex) {
                        Logger.getLogger(Kick.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return;
        }

        if (str.length() <= 0) {

        } else {
            str.deleteCharAt(str.toString().length() - 1);
            String kickMessage = str.toString();

            if (args.length > 1) {
                player.disconnect(ChatColor.RED + kickMessage);
                for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                    if (all.hasPermission("bungee.kick.notify")) {
                        try {
                            all.sendMessage(Messages.TAG + Ranks.getRankColour(player) + player.getName() + " §cwas kicked by " + sender.getName() + " for " + kickMessage + ".");
                        } catch (SQLException ex) {
                            Logger.getLogger(Kick.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }

    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 2 || args.length == 0) {
            return ImmutableSet.of();
        }
        
        Set<String> matches = new HashSet<>();
        if(args.length == 1){
            String search = args[0].toLowerCase();
            for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
                if(all.getName().toLowerCase().startsWith(search)){
                    matches.add(all.getName());
                }
            }
        }
        return matches;
    }
}
