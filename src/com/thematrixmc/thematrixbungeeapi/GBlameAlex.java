package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class GBlameAlex extends Command{
  
    public GBlameAlex()
  {
    super("gblamealex", "bungee.command.gblamealex", new String[] { "globalblamealex" });
  }
  
  @Override
  public void execute(CommandSender sender, String[] args)
  {
    for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
        if(all.hasPermission("bungee.blamealex.receive")){
            all.sendMessage(Messages.TAG + "§3#BlameAlex.");
        }
    }
  }

  
}
