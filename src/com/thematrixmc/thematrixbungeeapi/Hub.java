package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.*;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Hub extends Command{

    public Hub() {
        super("hub", "bungee.command.hub");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer) sender;
            player.connect(ProxyServer.getInstance().getServerInfo("hub"));
        }
    }
}