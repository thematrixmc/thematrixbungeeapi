package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

public class WhereAmI extends Command {
  public WhereAmI()
  {
    super("whereami", "bungee.command.whereami", new String[] { "whatserveramion" });
  }
  
  @Override
  public void execute(CommandSender sender, String[] args)
  {
    String server = ProxyServer.getInstance().getPlayer(sender.getName()).getServer().getInfo().getName();
    if(server.contains("1") | server.contains("2") | server.contains("3") | server.contains("4") | server.contains("5") | server.contains("6") | server.contains("7") | server.contains("8") | server.contains("9") || server.contains("0")){
        sender.sendMessage(Messages.TAG + ChatColor.GREEN + "You are on " + server.toUpperCase() + ".");
    }else{
        sender.sendMessage(Messages.TAG + ChatColor.GREEN + "You are on " + server + ".");
    }   
  }
}
