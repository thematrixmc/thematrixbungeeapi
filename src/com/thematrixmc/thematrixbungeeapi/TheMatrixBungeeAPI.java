package com.thematrixmc.thematrixbungeeapi;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class TheMatrixBungeeAPI extends Plugin {

    public static Connection conn;
    public static Connection conn1;
    public static TheMatrixBungeeAPI plugin;

    public static HashMap<String, Boolean> staffChat = new HashMap<>();
    public static int ffa;
    //public static int tdm1;
    //public static int tdm2;
    //public static int tdm3;
    public static int hub;
    //public static int build;

    @Override
    public void onEnable() {
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GlobalSay());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GlobalWho());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Kick());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Staff());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new WhereAmI());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new WhereIs());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Ping());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GBlameAlex());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GlobalGetRank());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GlobalSetRank());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new GlobalList());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Hub());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new StaffChatOn());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new StaffChatOff());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new StaffChatNotifyOn());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new StaffChatNotifyOff());

        ProxyServer.getInstance().getPluginManager().registerListener(this, new Listeners());
        try {
            setupConfig();
            conn = DriverManager.getConnection(getMatrixConfig().getString("mysql.path"), getMatrixConfig().getString("mysql.user"), getMatrixConfig().getString("mysql.pass"));
            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                PermissionsAPI.setRank(all, PermissionsAPI.getBungeeRank(all));
            }
        } catch (IOException | SQLException | ClassNotFoundException ex) {
            System.out.println("Cannot connect to database! Proxy disabling...");
            ProxyServer.getInstance().stop();
        }

    }

    @Override
    public void onDisable() {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
        }
    }

    public void setupConfig() throws IOException {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File file = new File(getDataFolder(), "config.yml");

        if (!file.exists()) {
            Files.copy(getResourceAsStream("config.yml"), file.toPath());
        }
    }

    public Configuration getMatrixConfig() throws IOException {
        Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
        return configuration;
    }

    public void saveMatrixConfig() throws IOException {
        ConfigurationProvider.getProvider(YamlConfiguration.class).save(getMatrixConfig(), new File(getDataFolder(), "config.yml"));
    }

    public void globalList(CommandSender sender) throws IOException, SQLException {
        int global;
        
        if (ProxyServer.getInstance().getOnlineCount() == 0 || ProxyServer.getInstance().getPlayers() == null) {
            global = 0;
        } else {
            global = ProxyServer.getInstance().getOnlineCount();
        }
        int staff = 0;

        for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
            try {
                if (Ranks.isStaff(all)) {
                    staff++;  
                }
            } catch (SQLException ex) {
                Logger.getLogger(GlobalList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        sender.sendMessage(Messages.TAG + "§eProxy Stats:");
        sender.sendMessage(Messages.TAG + "§aGlobal player count: " + global);
        sender.sendMessage(Messages.TAG + "§aStaff player count: " + staff);

        
    }

}
