package com.thematrixmc.thematrixbungeeapi;

import java.sql.SQLException;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Listeners implements Listener{
    
    @EventHandler
    public void onPostLogin(PostLoginEvent event) throws SQLException, ClassNotFoundException{
        if(PermissionsAPI.getBungeeRank(event.getPlayer()) == null){
            Ranks.setBungeePermissions(event.getPlayer(), "default");
            PermissionsAPI.setRank(event.getPlayer(), "default");
            PermissionsAPI.setOfflineRank(event.getPlayer().getName(), "default");
        }else{
            Ranks.removeBungeePermissions(event.getPlayer(), PermissionsAPI.getBungeeRank(event.getPlayer()));
            Ranks.setBungeePermissions(event.getPlayer(), PermissionsAPI.getBungeeRank(event.getPlayer()));            
        }
        
        TheMatrixBungeeAPI.staffChat.put(event.getPlayer().getName(), false); 
        
    }
    
    @EventHandler
    public void onChat(ChatEvent event){
        if(event.getSender() instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer) event.getSender();
            if(TheMatrixBungeeAPI.staffChat.get(player.getName()) == true){
                String msg = event.getMessage();
                if(!msg.startsWith("/")){
                    event.setMessage("/s " + msg);
                }
            }
        }
    }
}
