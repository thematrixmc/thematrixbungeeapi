package com.thematrixmc.thematrixbungeeapi;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class GlobalList extends Command{

  public GlobalList() {
    super("globallist", "bungee.command.globallist", new String[] { "glist" });
  }
  
  TheMatrixBungeeAPI Core = new TheMatrixBungeeAPI();
  
  @Override
  public void execute(CommandSender sender, String[] args) {
      if(args.length > 1){
          sender.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /globallist or /glist");
      }
      
      try {
          Core.globalList(sender);
      } catch (IOException | SQLException ex) {
          Logger.getLogger(GlobalList.class.getName()).log(Level.SEVERE, null, ex);
      }
     
  }
}
