package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

public class GlobalSay extends Command {
    
  public GlobalSay(){
    super("globalsay", "bungee.command.globalsay", new String[] { "bungeesay" });
  }
  
  @Override
  public void execute(CommandSender sender, String[] args){
    if (args.length == 0){
      sender.sendMessage(Messages.TAG + ChatColor.DARK_AQUA + "Usage: /globalsay [message]");
    }else{
      if (args.length < 1) {
        return;
      }
      String cast = "";
      for (String str : args) {
        cast = cast + str + " ";
      }
      ProxyServer.getInstance().broadcast(ChatColor.YELLOW + "[GLOBAL] " + ChatColor.GOLD + sender.getName() + ChatColor.DARK_GRAY + " ❚ " + ChatColor.GREEN + cast);
    }
  }
}
