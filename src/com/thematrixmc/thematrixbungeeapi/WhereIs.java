package com.thematrixmc.thematrixbungeeapi;

import com.google.common.collect.ImmutableSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class WhereIs extends Command implements TabExecutor {

    public WhereIs() {
        super("whereis", "bungee.command.whereis", new String[]{"find"});
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(Messages.TAG + ChatColor.RED + "Usage: /whereis [user]");
            return;
        }
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
        if (player == null) {
            sender.sendMessage(Messages.TAG + ChatColor.RED + args[0] + " is offline.");
            return;
        }
        try {
            Server server = ProxyServer.getInstance().getPlayer(player.getName()).getServer();
            String serverName = server.toString();
            if (serverName.contains("1") | serverName.contains("2") | serverName.contains("3") | serverName.contains("4") | serverName.contains("5") | serverName.contains("6") | serverName.contains("7") | serverName.contains("8") | serverName.contains("9") || serverName.contains("0")) {
                sender.sendMessage(Messages.TAG + "§aYou are on §c" + serverName.toUpperCase() + ".");
            } else {
                sender.sendMessage(Messages.TAG + Ranks.getRankColour(player) + player.getName() + " §ais on §c" + serverName + ".");
            }
        } catch (SQLException ex) {
            Logger.getLogger(WhereIs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 1 || args.length == 0) {
            return ImmutableSet.of();
        }

        Set<String> matches = new HashSet<>();
        if (args.length == 1) {
            String search = args[0].toLowerCase();
            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                if (all.getName().toLowerCase().startsWith(search)) {
                    matches.add(all.getName());
                }
            }
        }
        return matches;
    }
}
