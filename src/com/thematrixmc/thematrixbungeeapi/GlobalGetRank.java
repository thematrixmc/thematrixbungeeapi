package com.thematrixmc.thematrixbungeeapi;

import com.google.common.collect.ImmutableSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class GlobalGetRank extends Command implements TabExecutor{
    
    public GlobalGetRank(){
        super("ggetrank", "bungee.command.ggetrank", new String[] { "globalgetrank" });
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 0 || args.length > 2) {
            sender.sendMessage(Messages.TAG + ChatColor.RED + "Incorrect syntax. Please use: /ggetrank [user]");
            return;
        }
        
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
        if (player == null){
            sender.sendMessage(Messages.TAG + ChatColor.RED + args[0] +  " is offline.");
        }else{
            try {
                if(Ranks.getRank(player) == null){
                    sender.sendMessage(Messages.TAG + "§aPlayer §9" + player.getName() + "§a is rank §9Default§a.");
                }else{
                    sender.sendMessage(Messages.TAG + "§aPlayer " + player.getName() + "§a is rank " + Ranks.getRankColour(player) + Ranks.getRank(player) + "§a.");
                }
            } catch (SQLException ex) {
                Logger.getLogger(GlobalGetRank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }      

    }
    
    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 2 || args.length == 0) {
            return ImmutableSet.of();
        }
        
        Set<String> matches = new HashSet<>();
        if(args.length == 1){
            String search = args[0].toLowerCase();
            for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
                if(all.getName().toLowerCase().startsWith(search)){
                    matches.add(all.getName());
                }
            }
        }
        return matches;
    }
}