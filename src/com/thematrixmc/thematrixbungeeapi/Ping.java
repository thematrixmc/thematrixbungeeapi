package com.thematrixmc.thematrixbungeeapi;

import com.google.common.collect.ImmutableSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class Ping extends Command implements TabExecutor{

    public Ping() {
        super("ping", "bungee.command.ping", new String[]{"proxyping", "ping2proxy"});
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(Messages.TAG + ChatColor.GREEN + "Your ping is: " + ProxyServer.getInstance().getPlayer(sender.getName()).getPing());
            return;
        }
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
        if (player == null) {
            sender.sendMessage(Messages.TAG + ChatColor.RED + "Player is not online.");
            return;
        }
        try {
            sender.sendMessage(Messages.TAG + Ranks.getRankColour(player) + player.getName() + ChatColor.GREEN + "'s ping is: " + player.getPing());
        } catch (SQLException ex) {
            Logger.getLogger(Ping.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 1 || args.length == 0) {
            return ImmutableSet.of();
        }
        
        Set<String> matches = new HashSet<>();
        if(args.length == 1){
            String search = args[0].toLowerCase();
            for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
                if(all.getName().toLowerCase().startsWith(search)){
                    matches.add(all.getName());
                }
            }
        }
        return matches;
    }
}
