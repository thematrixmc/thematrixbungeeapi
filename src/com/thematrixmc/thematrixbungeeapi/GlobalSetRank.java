package com.thematrixmc.thematrixbungeeapi;

import com.google.common.collect.ImmutableSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class GlobalSetRank extends Command implements TabExecutor{

    public GlobalSetRank() {
        super("gsetrank", "bungee.command.gsetrank", new String[]{"globalsetrank"});
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0 || args.length > 2) {
            sender.sendMessage(Messages.TAG + ChatColor.RED + "Incorrect syntax. Please use: /gsetrank [user] [rank]");
            return;
        }

        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
        String rank = args[1];

        if (player == null) {
            sender.sendMessage(Messages.TAG + "§cThat player is offline.");
        } else {
            try {
                PermissionsAPI.setRank(player, rank);
                sender.sendMessage(Messages.TAG + Ranks.getRankColour(player) + player.getName() + "'s §arank updated.");
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(GlobalSetRank.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
    
    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length > 2 || args.length == 0) {
            return ImmutableSet.of();
        }
        
        Set<String> matches = new HashSet<>();
        if(args.length == 1){
            String search = args[0].toLowerCase();
            for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()){
                if(all.getName().toLowerCase().startsWith(search)){
                    matches.add(all.getName());
                }
            }
        }
        return matches;
    }
}
