package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class StaffChatNotifyOn extends Command{

    public StaffChatNotifyOn() {
        super("snon", "bungee.staffchat.notifyon", "staffnotifyon");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if(args.length > 0){
                player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /staffnotifyon");
            }else{
                if(player.hasPermission("bungee.notify.staff")){
                    player.sendMessage(Messages.TAG + "§cYour staff chat notifications is already toggled on.");
                }else{
                    player.setPermission("bungee.notify.staff", true);
                    player.sendMessage(Messages.TAG + "§aStaff chat notifications has successfully toggled: §lon.");
                }
            }
        }
    }
    
    

}
