package com.thematrixmc.thematrixbungeeapi;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class StaffChatNotifyOff extends Command{

    public StaffChatNotifyOff() {
        super("snoff", "bungee.staffchat.notifyoff", "staffnotifyoff");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(sender instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if(args.length > 0){
                player.sendMessage(Messages.TAG + "§cIncorrect syntax. Please use: /staffnotifyoff");
            }else{
                if(!player.hasPermission("bungee.notify.staff")){
                    player.sendMessage(Messages.TAG + "§cYour staff chat notifications is already toggled off.");
                }else{
                    player.setPermission("bungee.notify.staff", false);
                    player.sendMessage(Messages.TAG + "§cStaff chat notifications has successfully toggled: §loff.");
                }
            }
        }
    }
    
    

}
