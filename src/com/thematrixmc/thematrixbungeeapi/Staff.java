package com.thematrixmc.thematrixbungeeapi;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Staff
  extends Command
{
  public Staff()
  {
    super("staff", "bungee.command.staff", new String[] { "s" });
  }
  
  @Override
  public void execute(CommandSender sender, String[] args)
  {
    String cast;
    if (args.length == 0)
    {
        sender.sendMessage(Messages.TAG + ChatColor.RED + "Usage: /staff [message]");
    }
    else
    {
      if (args.length < 1) {
        return;
      }
      cast = "";
      for (String str : args) {
        cast = cast + str + " ";
      }
      for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
        if (player.hasPermission("bungee.notify.staff")) {
            try {
                player.sendMessage("§4§lSTAFF §8\u275a " + Ranks.getRankColour((ProxiedPlayer) sender) + sender.getName() + " §7» " + ChatColor.RESET + cast);
            } catch (SQLException ex) {
                Logger.getLogger(Staff.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
      }
    }
  }
}
